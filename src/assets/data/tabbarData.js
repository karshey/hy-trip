const tabbarData=[
    {
        image:'tab_home.png',
        imageActive:'tab_home_active.png',
        text:'首页',
        path:'/home'
    },
    {
        image:'tab_favor.png',
        imageActive:'tab_favor_active.png',
        text:'收藏',
        path:'/favor'
    },
    {
        image:'tab_order.png',
        imageActive:'tab_order_active.png',
        text:'订单',
        path:'/order'
    },
    {
        image:'tab_message.png',
        imageActive:'tab_message.png',
        text:'消息',
        path:'/message'
    }
]

export default tabbarData