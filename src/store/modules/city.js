// city.vue页面所有的进行网络请求和数据都封装到这里
import { getAllCity } from "@/service";
import { defineStore } from "pinia";

const useCityStore = defineStore('city', {
    state: () => {
        return {
            allCity: {},
            currentCity:{
                // 默认值
                cityName:'广州',
            }
        }
    },
    actions: {
        // 调用网络请求
        async fetchAllCity() {
            const res = await getAllCity()
            this.allCity = res.data
        }
    }
})

export default useCityStore