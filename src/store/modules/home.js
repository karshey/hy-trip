// home.vue页面所有的进行网络请求和数据都封装到这里

import { defineStore } from "pinia";
import { getHotSuggest, getCategories, getHouseList } from '@/service'

const useHomeStore = defineStore('home', {
    state: () => {
        return {
            hotSuggest: [],
            categories: [],
            houseList: [],
            currentPage: 1,

        }
    },
    actions: {
        // 网络请求,由于返回一个promise,要异步async await
        async fetchHotSuggest() {
            const res = await getHotSuggest()
            this.hotSuggest = res.data
            // console.log(res)
        },
        async fetchCategories() {
            const res = await getCategories()
            this.categories = res.data
        },
        async fetchHouseList() {
            const res = await getHouseList(this.currentPage)
            this.currentPage++
            this.houseList.push(...res.data)
        }
    }
})

export default useHomeStore