import { defineStore } from "pinia";
import { getDetailInfos } from '@/service/modules/detail'

const useDetailStore = defineStore('detail', {
    state: () => {
        return {
            detailData: {},

        }
    },
    actions: {
        async fetchDetailData(houseId) {
            const res = await getDetailInfos(houseId)
            // console.log(res)
            this.detailData=res.data
        }
    }
})

export default useDetailStore