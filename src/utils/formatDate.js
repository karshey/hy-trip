import dayjs from 'dayjs'

// 格式化“x月x日”
export function formatMonthDay(date) {
    return dayjs(date).format('MM月DD日')
}

// 格式化“x.x”
export function formatMonthDay2(date) {
    return dayjs(date).format('MM.DD')
}

// end-start
export function getDiffDate(start, end) {
    return dayjs(end).diff(start, 'day')
}