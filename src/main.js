import { createApp } from 'vue'
import App from './App.vue'
import "normalize.css"
import './assets/css/index.css'
import router from './router'
import pinia from './store'
// 引入vant组件
import { NavBar, SwipeItem, Swipe, Icon, Rate, Calendar, Cell, IndexBar, IndexAnchor, Tab, Tabs, Search, Tabbar, TabbarItem, Button } from 'vant';
// 引入组件样式
import 'vant/lib/index.css';

const app = createApp(App)

app.use(router).use(pinia)
app.use(Button).use(Tabbar).use(TabbarItem).use(Search).use(Tab).use(Tabs).use(IndexBar).use(IndexAnchor).use(Cell)
app.use(Calendar).use(Rate).use(Icon).use(SwipeItem).use(Swipe).use(NavBar)

app.mount('#app')
