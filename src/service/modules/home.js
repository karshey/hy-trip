// 此文件保存所有home页面的网络请求
import HYRequest from '@/service/request'

export function getHotSuggest() {
    // request的index导出的是一个对象
    return HYRequest.get({
        // 参数也是一个对象
        url: '/home/hotSuggests'
    })
}

export function getCategories() {
    // request的index导出的是一个对象
    return HYRequest.get({
        // 参数也是一个对象
        url: '/home/categories'
    })
}

export function getHouseList(currentPage) {
    return HYRequest.get({
        url: '/home/houselist',
        params: {
            page: currentPage
        }
    })
}