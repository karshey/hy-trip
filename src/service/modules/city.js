// 此文件保存所有city页面的网络请求
import HYRequest from '@/service/request'

export function getAllCity() {
    // request的index导出的是一个对象
    return HYRequest.get({
        // 参数也是一个对象
        url: '/city/all'
    })
}

