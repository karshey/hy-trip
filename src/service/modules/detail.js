// 此文件保存所有detail页面的网络请求
import HYRequest from '../request'

export function getDetailInfos(houseId) {
    return HYRequest.get({
        url: '/detail/infos',
        params: {
            houseId
        }
    })
}
