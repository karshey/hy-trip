import axios from "axios";

import { useLoadingStore } from "@/store/modules/loading";
import useMainStore from "../../store/modules/main";
import { baseURL, TIMEOUT } from "./config";

const loadingStore = useLoadingStore();
const mainStore = useMainStore()

class HYRequest {
  constructor(baseURL) {
    this.instance = axios.create({
      baseURL,
      timeout: TIMEOUT,
    });

    this.instance.interceptors.request.use(config => {
      // 发送网络请求前要做的事
      mainStore.isLoading = true
      return config
    }, err => {
      // 发送网络请求失败要做的事
      return err
    })

    this.instance.interceptors.response.use(res => {
      // 响应成功要做的事
      mainStore.isLoading = false
      return res
    }, err => {
      // 响应失败要做的事
      mainStore.isLoading = false
      return err
    })
  }

  request(config) {
    loadingStore.changeLoading(true);
    return new Promise((resolve, reject) => {
      this.instance
        .request(config)
        .then((res) => {
          resolve(res.data);
        })
        .catch((err) => {
          console.log("request err:", err);
          reject(err);
        })
        .finally(() => {
          loadingStore.changeLoading(false);
        });
    });
  }

  get(config) {
    return this.request({ ...config, method: "get" });
  }

  post(config) {
    return this.request({ ...config, method: "post" });
  }
}

export default new HYRequest(baseURL);
