# 项目相关说明

[【前端】Vue3+Vant4项目：旅游App-项目总结与预览（已开源）_karshey的博客-CSDN博客](https://blog.csdn.net/karshey/article/details/129110506)

[【前端】Vue3+Vant4项目：旅游App-项目笔记总结（目录）_karshey的博客-CSDN博客](https://blog.csdn.net/karshey/article/details/128497447)

# hy-trip2

This template should help get you started developing with Vue 3 in Vite.

## Recommended IDE Setup

[VSCode](https://code.visualstudio.com/) + [Volar](https://marketplace.visualstudio.com/items?itemName=Vue.volar) (and disable Vetur) + [TypeScript Vue Plugin (Volar)](https://marketplace.visualstudio.com/items?itemName=Vue.vscode-typescript-vue-plugin).

## Customize configuration

See [Vite Configuration Reference](https://vitejs.dev/config/).

## Project Setup

```sh
npm install
```

### Compile and Hot-Reload for Development

```sh
npm run dev
```

### Compile and Minify for Production

```sh
npm run build
```
